# -*- coding: utf-8 -*-
#
# This file is part of the RingSimulator project
#
# Copyright (C): 2019
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the LGPL license.
# See LICENSE.txt for more info.

"""
RingSimulator

This class is the interface to the ESRF EBS storage ring simulator.
Simulation is done using the python Accelerator Toolbox (pyAT) 
package
"""

# PROTECTED REGION ID(RingSimulator.system_imports) ENABLED START #
# PROTECTED REGION END #    //  RingSimulator.system_imports

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
import enum
# Additional import
# PROTECTED REGION ID(RingSimulator.additionnal_import) ENABLED START #
import multiprocessing
import time
import importlib
import sys
import os
import numpy as np
# PROTECTED REGION END #    //  RingSimulator.additionnal_import

__all__ = ["RingSimulator", "main"]


class Mode(enum.IntEnum):
    """Python enumerated type for Mode attribute."""
    TbT = 0
    Atlinopt = 1
    Atx = 2
    Mixed = 3


class RingSimulator(Device):
    """
    This class is the interface to the ESRF EBS storage ring simulator.
    Simulation is done using the python Accelerator Toolbox (pyAT) 
    package

    **Properties:**

    - Device Property
        Path
            - File system path to find out the Python module root folder
            - Type:'DevString'
        FunctionName
            - The main simulation loop function name
            - Type:'DevString'
        ModuleName
            - The Python module name (Related to Path)
            - Type:'DevString'
        RingFile
            - The ring description file
            - Type:'DevString'
        DesignRing
            - Variable name containing the design ring description
            - Type:'DevString'
        ErrorRing
            - Variable name containing the design ring description with errors
            - Type:'DevString'
        CorrectionRing
            - Variable name containing the design ring description with errors\nand correction
            - Type:'DevString'
        Verbose
            - Verbose mode for the simulation loop
            - Type:'DevBoolean'
        MonitoredDevices
            - List of monitored magnet devices
            - Type:'DevVarStringArray'
        StrengthOffsets
            - List of strenght offsets (same length as moniroed devices)
            - Type:'DevVarDoubleArray'
        RFOffset
            - RF Offset (Hz)
            - Type:'DevDouble'
    """
    # PROTECTED REGION ID(RingSimulator.class_variable) ENABLED START #

    NB_BPM = 320
    NB_PINHOLES = 5
    NB_DATA_PINHOLES = 5
    ATLINOPT_NB_DATA = 6 + (6 * NB_BPM) + (2 * NB_PINHOLES)
    TBT_COORD_NB = 6

    # PROTECTED REGION END #    //  RingSimulator.class_variable

    # -----------------
    # Device Properties
    # -----------------

    Path = device_property(
        dtype='DevString',
        mandatory=True
    )

    FunctionName = device_property(
        dtype='DevString',
        mandatory=True
    )

    ModuleName = device_property(
        dtype='DevString',
        mandatory=True
    )

    RingFile = device_property(
        dtype='DevString',
    )

    DesignRing = device_property(
        dtype='DevString',
    )

    ErrorRing = device_property(
        dtype='DevString',
    )

    CorrectionRing = device_property(
        dtype='DevString',
    )

    Verbose = device_property(
        dtype='DevBoolean',
        default_value=False
    )

    MonitoredDevices = device_property(
        dtype='DevVarStringArray',
    )

    StrengthOffsets = device_property(
        dtype='DevVarDoubleArray',
    )

    RFOffset = device_property(
        dtype='DevDouble',
        default_value=0
    )

    # ----------
    # Attributes
    # ----------

    Tune_h = attribute(
        dtype='DevDouble',
        label="Horizontal tune",
    )

    Tune_v = attribute(
        dtype='DevDouble',
        label="Vertical tune",
    )

    Chromaticity_h = attribute(
        dtype='DevDouble',
        label="Horizontal chromaticity",
    )

    Chromaticity_v = attribute(
        dtype='DevDouble',
        label="Vertical chromaticity",
    )

    Emittance_h = attribute(
        dtype='DevDouble',
        label="Horizontal emittance",
        unit="pm",
        standard_unit="1",
        display_unit="1e12",
        doc="Horizontal emittance",
    )

    Emittance_v = attribute(
        dtype='DevDouble',
        label="Vertical emittance",
        unit="pm",
        standard_unit="1.0",
        display_unit="1e12",
        doc="Vertical emittance",
    )

    RfFrequency = attribute(
        dtype='DevDouble',
        access=AttrWriteType.READ_WRITE,
        label="Rf Frequency",
        unit="MHz",
        standard_unit="1.0",
        display_unit="1e-06",
        format="%12.8f",
        max_value=359000000,
    )

    RfVoltage = attribute(
        dtype='DevDouble',
        access=AttrWriteType.READ_WRITE,
        label="Rf Voltage",
        unit="MV",
        standard_unit="1.0",
        display_unit="1e-06",
        format="%8.6f",
        max_value=9000000,
    )

    Radiation = attribute(
        dtype='DevBoolean',
        access=AttrWriteType.READ_WRITE,
    )

    LoopTime = attribute(
        dtype='DevDouble',
        unit="s",
        standard_unit="1",
        display_unit="1",
        doc="Py AT loop time (in sec)",
    )

    Counter = attribute(
        dtype='DevLong64',
        display_level=DispLevel.EXPERT,
        label="SimuLoopCounter",
        doc="Simulator loop counter",
    )

    Mode = attribute(
        dtype=Mode,
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        doc="Simulator mode",
    )

    AtxEvery = attribute(
        dtype='DevLong',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        unit="loop",
        max_value=100,
        min_value=1,
        memorized=True,
        hw_memorized=True,
        doc="n mixed mode, one call to atx every XX loops",
    )

    RingName = attribute(
        dtype='DevString',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        doc="Ring name from Py AT",
    )

    SavedFile = attribute(
        dtype='DevString',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        memorized=True,
        hw_memorized=True,
    )

    SimulationStarted = attribute(
        dtype='DevBoolean',
        display_level=DispLevel.EXPERT,
        doc="Set to true when the simulation loop is MOVING. Change event is fired on this attribute",
    )

    TbT_BufferSize = attribute(
        dtype='DevLong',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        unit="turn",
        max_value=5000,
        min_value=-5000,
        doc="Buffer size (in turn number) for the Turn By Turn mode",
    )

    BpmNoise = attribute(
        dtype='DevDouble',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        max_value=1.0,
        min_value=0.0,
        memorized=True,
        hw_memorized=True,
    )

    QuadGradJit = attribute(
        dtype='DevDouble',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        label="Quad Gradient Jitter",
        format="%5.4f",
        memorized=True,
        hw_memorized=True,
    )

    QuadVibAmpl = attribute(
        dtype='DevDouble',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        label="Quad Vibration Amplitude",
        unit="um",
        standard_unit="1.0",
        display_unit="1e06",
        format="%5.3f",
        memorized=True,
        hw_memorized=True,
    )

    ErrMessage = attribute(
        dtype='DevString',
        access=AttrWriteType.WRITE,
        display_level=DispLevel.EXPERT,
    )

    ClosedOrbit_h = attribute(
        dtype=('DevDouble',),
        max_dim_x=320,
        label="Horizontal closed orbit",
        unit="m",
    )

    ClosedOrbit_v = attribute(
        dtype=('DevDouble',),
        max_dim_x=320,
        label="Vertical closed orbit",
        unit="m",
    )

    Beta_h = attribute(
        dtype=('DevDouble',),
        max_dim_x=320,
        label="Horizontal beta function",
        unit="m",
    )

    Beta_v = attribute(
        dtype=('DevDouble',),
        max_dim_x=320,
        label="Vertical beta function",
        unit="m",
    )

    Eta_h = attribute(
        dtype=('DevDouble',),
        max_dim_x=320,
        label="Horizontal dispersion function",
        unit="m",
    )

    Eta_v = attribute(
        dtype=('DevDouble',),
        max_dim_x=320,
        label="Vertical dispersion function",
        unit="m",
    )

    TbT_InCoord = attribute(
        dtype=('DevDouble',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=6,
        display_level=DispLevel.EXPERT,
        label="TbTInputCoordinates",
        doc="arr[0] = x, arr[1] = x`, arr[2] = z, arr[3] = z`. arr[4] = dp/p, arr[5] = ct",
    )

    Emittances_BPM_H = attribute(
        dtype=('DevDouble',),
        max_dim_x=320,
    )

    Emittances_BPM_V = attribute(
        dtype=('DevDouble',),
        max_dim_x=320,
    )

    Emittances_PinHoles_H = attribute(
        dtype=('DevDouble',),
        max_dim_x=5,
    )

    Emittances_PinHoles_V = attribute(
        dtype=('DevDouble',),
        max_dim_x=5,
    )

    S_bpms = attribute(
        dtype=('DevDouble',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=320,
    )

    S_pinholes = attribute(
        dtype=('DevDouble',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=5,
    )

    HPositionsTbT = attribute(
        dtype=(('DevDouble',),),
        max_dim_x=5000, max_dim_y=320,
        unit="mm",
        standard_unit="1.0",
        display_unit="1000.0",
        format="%6.2f",
        doc="H positions in TbT mode. In the iimage, the X dimension is the turn number while the \nY dimension is the BPM number",
    )

    VPositionsTbT = attribute(
        dtype=(('DevDouble',),),
        max_dim_x=5000, max_dim_y=320,
        unit="mm",
        standard_unit="1.0",
        display_unit="1000.0",
        format="%6.2f",
        doc="V positions in TbT mode. In the image, the X dimension is the turn number while the \nY dimension is the BPM number",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initializes the attributes and properties of the RingSimulator."""
        Device.init_device(self)
        self.Mode.set_data_ready_event(True)
        self.set_change_event("SavedFile", True, False)
        self.set_change_event("SimulationStarted", True, False)
        self.__tune_h = 0.0
        self.__tune_v = 0.0
        self.__chromaticity_h = 0.0
        self.__chromaticity_v = 0.0
        self.__emittance_h = 0.0
        self.__emittance_v = 0.0
        self.__rf_frequency = 0.0
        self.__rf_voltage = 0.0
        self.__radiation = False
        self.__loop_time = 0.0
        self.__counter = 0
        self.__mode = Mode.TbT
        self.__atx_every = 0
        self.__ring_name = ''
        self.__saved_file = ''
        self.__simulation_started = False
        self.__tb_t__buffer_size = 0
        self.__bpm_noise = 0.0
        self.__quad_grad_jit = 0.0
        self.__quad_vib_ampl = 0.0
        self.__err_message = ''
        self.__closed_orbit_h = (0.0,)
        self.__closed_orbit_v = (0.0,)
        self.__beta_h = (0.0,)
        self.__beta_v = (0.0,)
        self.__eta_h = (0.0,)
        self.__eta_v = (0.0,)
        self.__tb_t__in_coord = (0.0,)
        self.__emittances__bpm__h = np.zeros(RingSimulator.NB_BPM)
        self.__emittances__bpm__v = np.zeros(RingSimulator.NB_BPM)
        self.__emittances__pin_holes__h = np.zeros(RingSimulator.NB_PINHOLES)
        self.__emittances__pin_holes__v = np.zeros(RingSimulator.NB_PINHOLES)
        self._s_bpms = np.zeros(RingSimulator.NB_BPM)
        self._s_pinholes = np.zeros(RingSimulator.NB_PINHOLES)
        self._h_positions_tb_t = ((0.0,),)
        self._v_positions_tb_t = ((0.0,),)
        # PROTECTED REGION ID(RingSimulator.init_device) ENABLED START #
        self.__atx_every = 5
        self.__tune_h = 0.0
        self.__tune_v = 0.0
        self.__chromaticity_h = 0.0
        self.__chromaticity_v = 0.0
        self.__emittance_h = 0.0
        self.__emittance_v = 0.0
        self.__rf_frequency = 0.0
        self.__rf_voltage = 0.0
        self.__radiation = False
        self.__loop_time = 0.0
        self.__counter = 0
        self.__mode = Mode.Mixed
        self.__ring_name = ''
        self.__simulation_started = False
        self.__tb_t__buffer_size = 0
        self.__bpm_noise = 0.0
        self.__quad_grad_jit = 0.0
        self.__quad_vib_ampl = 0.0
        self.__err_message = ''
        self.__magnet_length = (0.0,)
        self.__closed_orbit_h = np.zeros(RingSimulator.NB_BPM)
        self.__closed_orbit_v = np.zeros(RingSimulator.NB_BPM)
        self.__beta_h = np.zeros(RingSimulator.NB_BPM)
        self.__beta_v = np.zeros(RingSimulator.NB_BPM)
        self.__eta_h = np.zeros(RingSimulator.NB_BPM)
        self.__eta_v = np.zeros(RingSimulator.NB_BPM)
        self.__tb_t__in_coord = (0.0,)
        
        self.__h_positions_tb_t = np.zeros(1)
        self.__v_positions_tb_t = np.zeros(1)

        self.p = None
        self.rfv_written = False
        self.cmd_tp = 0
        self.prev_cmd_tp = 0

        self.updated_str = []
        self.updated_dev = []

        self.pinholes_data = []
        for i in range(RingSimulator.NB_PINHOLES):
            self.pinholes_data.append([0.0]*RingSimulator.NB_DATA_PINHOLES)

        self.Mode.set_write_value(Mode.Mixed)

        sys.path.insert(0,self.Path)

        l1 = 0 if self.MonitoredDevices is None else len(self.MonitoredDevices)
        l2 = 0 if self.StrengthOffsets is None else len(self.StrengthOffsets)
        if l1 != l2:
            print("MonitoredDevices and StrengthOffsets properties must have the same length")
            os._exit(os.EX_OK)
        self.strengthOffsetMap = dict(zip(self.MonitoredDevices,self.StrengthOffsets)) if l1!=0 else None
        print(self.strengthOffsetMap)

        try:
            module_ = importlib.import_module(self.ModuleName)
            self.sim_loop_name = getattr(module_, self.FunctionName)

            self.p = multiprocessing.Process(target=self.sim_loop_name,args=(self.get_name(),self.RingFile,self.DesignRing,self.ErrorRing,self.CorrectionRing,self.Verbose,))
            self.p.start()

            self.set_state(DevState.INIT)
            self.set_status("Not initialised")
        except Exception as e:
#            print("Exception: ",str(e))
            except_str = "Exception during device init. Error message:\n" + str(e)
            self.set_state(tango.DevState.FAULT)
            self.__closed_orbit_h = np.empty(RingSimulator.NB_BPM)
            self.__closed_orbit_v = np.empty(RingSimulator.NB_BPM)
            self.__closed_orbit_h[:] = np.NaN
            self.__closed_orbit_v[:] = np.NaN
            self.set_status(except_str)

        # PROTECTED REGION END #    //  RingSimulator.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(RingSimulator.always_executed_hook) ENABLED START #

        s = self.get_state()
        if s == DevState.INIT or s == DevState.MOVING:
            if self.rfv_written is True:
                self.set_state(DevState.ON)
                self.__simulation_started = True
                self.push_change_event("SimulationStarted",self.__simulation_started)

        # PROTECTED REGION END #    //  RingSimulator.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(RingSimulator.delete_device) ENABLED START #

        if self.p is not None:
            self.p.kill()
            self.p.join()
            print("Process joined !!")

        # PROTECTED REGION END #    //  RingSimulator.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_Tune_h(self):
        # PROTECTED REGION ID(RingSimulator.Tune_h_read) ENABLED START #
        """Return the Tune_h attribute."""
        return self.__tune_h
        # PROTECTED REGION END #    //  RingSimulator.Tune_h_read
    def read_Tune_v(self):
        # PROTECTED REGION ID(RingSimulator.Tune_v_read) ENABLED START #
        """Return the Tune_v attribute."""
        return self.__tune_v
        # PROTECTED REGION END #    //  RingSimulator.Tune_v_read
    def read_Chromaticity_h(self):
        # PROTECTED REGION ID(RingSimulator.Chromaticity_h_read) ENABLED START #
        """Return the Chromaticity_h attribute."""
        return self.__chromaticity_h
        # PROTECTED REGION END #    //  RingSimulator.Chromaticity_h_read
    def read_Chromaticity_v(self):
        # PROTECTED REGION ID(RingSimulator.Chromaticity_v_read) ENABLED START #
        """Return the Chromaticity_v attribute."""
        return self.__chromaticity_v
        # PROTECTED REGION END #    //  RingSimulator.Chromaticity_v_read
    def read_Emittance_h(self):
        # PROTECTED REGION ID(RingSimulator.Emittance_h_read) ENABLED START #
        """Return the Emittance_h attribute."""
        return self.__emittance_h
        # PROTECTED REGION END #    //  RingSimulator.Emittance_h_read
    def read_Emittance_v(self):
        # PROTECTED REGION ID(RingSimulator.Emittance_v_read) ENABLED START #
        """Return the Emittance_v attribute."""
        return self.__emittance_v
        # PROTECTED REGION END #    //  RingSimulator.Emittance_v_read
    def read_RfFrequency(self):
        # PROTECTED REGION ID(RingSimulator.RfFrequency_read) ENABLED START #
        """Return the RfFrequency attribute."""
        return self.__rf_frequency
        # PROTECTED REGION END #    //  RingSimulator.RfFrequency_read
    def write_RfFrequency(self, value):
        # PROTECTED REGION ID(RingSimulator.RfFrequency_write) ENABLED START #
        """Set the RfFrequency attribute."""
        self.__rf_frequency = value - self.RFOffset
        # PROTECTED REGION END #    //  RingSimulator.RfFrequency_write
    def read_RfVoltage(self):
        # PROTECTED REGION ID(RingSimulator.RfVoltage_read) ENABLED START #
        """Return the RfVoltage attribute."""
        return self.__rf_voltage
        # PROTECTED REGION END #    //  RingSimulator.RfVoltage_read
    def write_RfVoltage(self, value):
        # PROTECTED REGION ID(RingSimulator.RfVoltage_write) ENABLED START #
        """Set the RfVoltage attribute."""
        self.__rf_voltage = value
        self.rfv_written = True
        # PROTECTED REGION END #    //  RingSimulator.RfVoltage_write
    def read_Radiation(self):
        # PROTECTED REGION ID(RingSimulator.Radiation_read) ENABLED START #
        """Return the Radiation attribute."""
        return self.__radiation
        # PROTECTED REGION END #    //  RingSimulator.Radiation_read
    def write_Radiation(self, value):
        # PROTECTED REGION ID(RingSimulator.Radiation_write) ENABLED START #
        """Set the Radiation attribute."""
        self.__radiation = value
        # PROTECTED REGION END #    //  RingSimulator.Radiation_write
    def read_LoopTime(self):
        # PROTECTED REGION ID(RingSimulator.LoopTime_read) ENABLED START #
        """Return the LoopTime attribute."""
        if self.__counter != 0:
            self.__loop_time = self.cmd_tp - self.prev_cmd_tp;
        else:
            self.__loop_time = 0.0;

        return self.__loop_time
        # PROTECTED REGION END #    //  RingSimulator.LoopTime_read
    def read_Counter(self):
        # PROTECTED REGION ID(RingSimulator.Counter_read) ENABLED START #
        """Return the Counter attribute."""
        return self.__counter
        # PROTECTED REGION END #    //  RingSimulator.Counter_read
    def read_Mode(self):
        # PROTECTED REGION ID(RingSimulator.Mode_read) ENABLED START #
        """Return the Mode attribute."""
        return self.__mode
        # PROTECTED REGION END #    //  RingSimulator.Mode_read
    def write_Mode(self, value):
        # PROTECTED REGION ID(RingSimulator.Mode_write) ENABLED START #
        """Set the Mode attribute."""
        self.__mode = value
        # PROTECTED REGION END #    //  RingSimulator.Mode_write
    def read_AtxEvery(self):
        # PROTECTED REGION ID(RingSimulator.AtxEvery_read) ENABLED START #
        """Return the AtxEvery attribute."""
        return self.__atx_every
        # PROTECTED REGION END #    //  RingSimulator.AtxEvery_read
    def write_AtxEvery(self, value):
        # PROTECTED REGION ID(RingSimulator.AtxEvery_write) ENABLED START #
        """Set the AtxEvery attribute."""
        self.__atx_every = value
        # PROTECTED REGION END #    //  RingSimulator.AtxEvery_write
    def read_RingName(self):
        # PROTECTED REGION ID(RingSimulator.RingName_read) ENABLED START #
        """Return the RingName attribute."""
        return self.__ring_name
        # PROTECTED REGION END #    //  RingSimulator.RingName_read
    def write_RingName(self, value):
        # PROTECTED REGION ID(RingSimulator.RingName_write) ENABLED START #
        """Set the RingName attribute."""
        self.__ring_name = value
        # PROTECTED REGION END #    //  RingSimulator.RingName_write
    def is_RingName_allowed(self, attr):
        # PROTECTED REGION ID(RingSimulator.is_RingName_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.FAULT,DevState.INIT]
        else:
            return self.get_state() not in [DevState.ON,DevState.OFF,DevState.FAULT]
        # PROTECTED REGION END #    //  RingSimulator.is_RingName_allowed

    def read_SavedFile(self):
        # PROTECTED REGION ID(RingSimulator.SavedFile_read) ENABLED START #
        """Return the SavedFile attribute."""
        return self.__saved_file
        # PROTECTED REGION END #    //  RingSimulator.SavedFile_read
    def write_SavedFile(self, value):
        # PROTECTED REGION ID(RingSimulator.SavedFile_write) ENABLED START #
        """Set the SavedFile attribute."""
        self.__saved_file = value
        util = tango.Util.instance()
        if util.is_svr_starting() is False:
            self.push_change_event("SavedFile",self.__saved_file)
        # PROTECTED REGION END #    //  RingSimulator.SavedFile_write
    def is_SavedFile_allowed(self, attr):
        # PROTECTED REGION ID(RingSimulator.is_SavedFile_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.FAULT]
        # PROTECTED REGION END #    //  RingSimulator.is_SavedFile_allowed

    def read_SimulationStarted(self):
        # PROTECTED REGION ID(RingSimulator.SimulationStarted_read) ENABLED START #
        """Return the SimulationStarted attribute."""
        return self.__simulation_started
        # PROTECTED REGION END #    //  RingSimulator.SimulationStarted_read
    def read_TbT_BufferSize(self):
        # PROTECTED REGION ID(RingSimulator.TbT_BufferSize_read) ENABLED START #
        """Return the TbT_BufferSize attribute."""
        return self.__tb_t__buffer_size
        # PROTECTED REGION END #    //  RingSimulator.TbT_BufferSize_read
    def write_TbT_BufferSize(self, value):
        # PROTECTED REGION ID(RingSimulator.TbT_BufferSize_write) ENABLED START #
        """Set the TbT_BufferSize attribute."""
        self.__tb_t__buffer_size = value
        # PROTECTED REGION END #    //  RingSimulator.TbT_BufferSize_write
    def read_BpmNoise(self):
        # PROTECTED REGION ID(RingSimulator.BpmNoise_read) ENABLED START #
        """Return the BpmNoise attribute."""
        return self.__bpm_noise
        # PROTECTED REGION END #    //  RingSimulator.BpmNoise_read
    def write_BpmNoise(self, value):
        # PROTECTED REGION ID(RingSimulator.BpmNoise_write) ENABLED START #
        """Set the BpmNoise attribute."""
        self.__bpm_noise = value
        # PROTECTED REGION END #    //  RingSimulator.BpmNoise_write
    def read_QuadGradJit(self):
        # PROTECTED REGION ID(RingSimulator.QuadGradJit_read) ENABLED START #
        """Return the QuadGradJit attribute."""
        return self.__quad_grad_jit
        # PROTECTED REGION END #    //  RingSimulator.QuadGradJit_read
    def write_QuadGradJit(self, value):
        # PROTECTED REGION ID(RingSimulator.QuadGradJit_write) ENABLED START #
        """Set the QuadGradJit attribute."""
        self.__quad_grad_jit = value
        # PROTECTED REGION END #    //  RingSimulator.QuadGradJit_write
    def read_QuadVibAmpl(self):
        # PROTECTED REGION ID(RingSimulator.QuadVibAmpl_read) ENABLED START #
        """Return the QuadVibAmpl attribute."""
        return self.__quad_vib_ampl
        # PROTECTED REGION END #    //  RingSimulator.QuadVibAmpl_read
    def write_QuadVibAmpl(self, value):
        # PROTECTED REGION ID(RingSimulator.QuadVibAmpl_write) ENABLED START #
        """Set the QuadVibAmpl attribute."""
        self.__quad_vib_ampl = value
        # PROTECTED REGION END #    //  RingSimulator.QuadVibAmpl_write
    def write_ErrMessage(self, value):
        # PROTECTED REGION ID(RingSimulator.ErrMessage_write) ENABLED START #
        """Set the ErrMessage attribute."""
        if( value=="" and self.get_state()==DevState.MOVING ):
            return
        self.__err_message = value
        if value == "" :
            self.set_status("The device is in ON state")
            self.set_state(DevState.ON)
        else:
            self.set_status(value)
            self.set_state(DevState.ALARM)
        # PROTECTED REGION END #    //  RingSimulator.ErrMessage_write
    def read_ClosedOrbit_h(self):
        # PROTECTED REGION ID(RingSimulator.ClosedOrbit_h_read) ENABLED START #
        """Return the ClosedOrbit_h attribute."""
        return self.__closed_orbit_h
        # PROTECTED REGION END #    //  RingSimulator.ClosedOrbit_h_read
    def read_ClosedOrbit_v(self):
        # PROTECTED REGION ID(RingSimulator.ClosedOrbit_v_read) ENABLED START #
        """Return the ClosedOrbit_v attribute."""
        return self.__closed_orbit_v
        # PROTECTED REGION END #    //  RingSimulator.ClosedOrbit_v_read
    def read_Beta_h(self):
        # PROTECTED REGION ID(RingSimulator.Beta_h_read) ENABLED START #
        """Return the Beta_h attribute."""
        return self.__beta_h
        # PROTECTED REGION END #    //  RingSimulator.Beta_h_read
    def read_Beta_v(self):
        # PROTECTED REGION ID(RingSimulator.Beta_v_read) ENABLED START #
        """Return the Beta_v attribute."""
        return self.__beta_v
        # PROTECTED REGION END #    //  RingSimulator.Beta_v_read
    def read_Eta_h(self):
        # PROTECTED REGION ID(RingSimulator.Eta_h_read) ENABLED START #
        """Return the Eta_h attribute."""
        return self.__eta_h
        # PROTECTED REGION END #    //  RingSimulator.Eta_h_read
    def read_Eta_v(self):
        # PROTECTED REGION ID(RingSimulator.Eta_v_read) ENABLED START #
        """Return the Eta_v attribute."""
        return self.__eta_v
        # PROTECTED REGION END #    //  RingSimulator.Eta_v_read
    def read_TbT_InCoord(self):
        # PROTECTED REGION ID(RingSimulator.TbT_InCoord_read) ENABLED START #
        """Return the TbT_InCoord attribute."""
        return self.__tb_t__in_coord
        # PROTECTED REGION END #    //  RingSimulator.TbT_InCoord_read
    def write_TbT_InCoord(self, value):
        # PROTECTED REGION ID(RingSimulator.TbT_InCoord_write) ENABLED START #
        """Set the TbT_InCoord attribute."""
        if len(value) != RingSimulator.TBT_COORD_NB:
            mess = "Wrong number of elements. You sent {} data while we need {}".format(len(value),RingSimulator.TBT_COORD_NB)
            tango.Except.throw_exception("RingSimulator_WrongArg",mess,"RingSimulator.write_TbT_InCoord()")

        self.__tb_t__in_coord = value

        # PROTECTED REGION END #    //  RingSimulator.TbT_InCoord_write
    def read_Emittances_BPM_H(self):
        # PROTECTED REGION ID(RingSimulator.Emittances_BPM_H_read) ENABLED START #
        """Return the Emittances_BPM_H attribute."""
        return self.__emittances__bpm__h
        # PROTECTED REGION END #    //  RingSimulator.Emittances_BPM_H_read
    def read_Emittances_BPM_V(self):
        # PROTECTED REGION ID(RingSimulator.Emittances_BPM_V_read) ENABLED START #
        """Return the Emittances_BPM_V attribute."""
        return self.__emittances__bpm__v
        # PROTECTED REGION END #    //  RingSimulator.Emittances_BPM_V_read
    def read_Emittances_PinHoles_H(self):
        # PROTECTED REGION ID(RingSimulator.Emittances_PinHoles_H_read) ENABLED START #
        """Return the Emittances_PinHoles_H attribute."""
        return self.__emittances__pin_holes__h
        # PROTECTED REGION END #    //  RingSimulator.Emittances_PinHoles_H_read
    def read_Emittances_PinHoles_V(self):
        # PROTECTED REGION ID(RingSimulator.Emittances_PinHoles_V_read) ENABLED START #
        """Return the Emittances_PinHoles_V attribute."""
        return self.__emittances__pin_holes__v
        # PROTECTED REGION END #    //  RingSimulator.Emittances_PinHoles_V_read
    def read_S_bpms(self):
        # PROTECTED REGION ID(RingSimulator.S_bpms_read) ENABLED START #
        """Return the S_bpms attribute."""
        return self._s_bpms
        # PROTECTED REGION END #    //  RingSimulator.S_bpms_read
    def write_S_bpms(self, value):
        # PROTECTED REGION ID(RingSimulator.S_bpms_write) ENABLED START #
        """Set the S_bpms attribute."""
        self._s_bpms = value
        # PROTECTED REGION END #    //  RingSimulator.S_bpms_write
    def read_S_pinholes(self):
        # PROTECTED REGION ID(RingSimulator.S_pinholes_read) ENABLED START #
        """Return the S_pinholes attribute."""
        return self._s_pinholes
        # PROTECTED REGION END #    //  RingSimulator.S_pinholes_read
    def write_S_pinholes(self, value):
        # PROTECTED REGION ID(RingSimulator.S_pinholes_write) ENABLED START #
        """Set the S_pinholes attribute."""
        self._s_pinholes = value
        # PROTECTED REGION END #    //  RingSimulator.S_pinholes_write
    def read_HPositionsTbT(self):
        # PROTECTED REGION ID(RingSimulator.HPositionsTbT_read) ENABLED START #
        """Return the HPositionsTbT attribute."""
        h2d = np.array(self.__h_positions_tb_t).reshape(self.NB_BPM, -1)
        return h2d
        # return self.__h_positions_tb_t
        # PROTECTED REGION END #    //  RingSimulator.HPositionsTbT_read
    def read_VPositionsTbT(self):
        # PROTECTED REGION ID(RingSimulator.VPositionsTbT_read) ENABLED START #
        """Return the VPositionsTbT attribute."""
        v2d = np.array(self.__v_positions_tb_t).reshape(self.NB_BPM, -1)
        return v2d
        # return self.__v_positions_tb_t
        # PROTECTED REGION END #    //  RingSimulator.VPositionsTbT_read
    # --------
    # Commands
    # --------

    @command(
    )
    @DebugIt()
    def On(self):
        # PROTECTED REGION ID(RingSimulator.On) ENABLED START #
        """
        Start the simulation

        :return:None
        """
        self.p = multiprocessing.Process(target=self.sim_loop_name,args=(self.get_name(),self.RingFile,self.DesignRing,self.ErrorRing,self.CorrectionRing,self.Verbose,))
        self.p.start()
        self.set_state(DevState.INIT)
        # PROTECTED REGION END #    //  RingSimulator.On

    def is_On_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_On_allowed) ENABLED START #
        return self.get_state() not in [DevState.ON,DevState.FAULT,DevState.INIT]
        # PROTECTED REGION END #    //  RingSimulator.is_On_allowed

    @command(
    )
    @DebugIt()
    def Off(self):
        # PROTECTED REGION ID(RingSimulator.Off) ENABLED START #
        """
        Stop the simulation

        :return:None
        """
        if self.p is not None:
            self.p.kill()
            self.p.join()
            self.set_status("Not initialised")
            self.set_state(DevState.OFF)
            self.p = None
            self.__simulation_started = False
            self.__counter = 0
            self.rfv_written = False

        # PROTECTED REGION END #    //  RingSimulator.Off

    def is_Off_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_Off_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.INIT]
        # PROTECTED REGION END #    //  RingSimulator.is_Off_allowed

    @command(
    )
    @DebugIt()
    def Reset(self):
        # PROTECTED REGION ID(RingSimulator.Reset) ENABLED START #
        """
        Reset the device after error

        :return:None
        """
        self.Off()
        self.__err_message = ''
        # PROTECTED REGION END #    //  RingSimulator.Reset

    def is_Reset_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_Reset_allowed) ENABLED START #
        return self.get_state() not in [DevState.ON,DevState.OFF,DevState.INIT]
        # PROTECTED REGION END #    //  RingSimulator.is_Reset_allowed

    @command(
    )
    @DebugIt()
    def ResetSimu(self):
        # PROTECTED REGION ID(RingSimulator.ResetSimu) ENABLED START #
        """
        Reset the whole simulation

        :return:None
        """
        self.set_status("Reset simulator in progress")
        self.push_data_ready_event("Mode",0)
        self.set_state(tango.DevState.MOVING)
        self.rfv_written = False
        # PROTECTED REGION END #    //  RingSimulator.ResetSimu

    def is_ResetSimu_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_ResetSimu_allowed) ENABLED START #
        return self.get_state() not in [DevState.FAULT,DevState.INIT,DevState.MOVING]
        # PROTECTED REGION END #    //  RingSimulator.is_ResetSimu_allowed

    @command(
        dtype_in='DevVarDoubleStringArray',
        doc_in="str[0..n] = Magnet name(s)"
               "db[0..n] = Magnet strength(s)",
        display_level=DispLevel.EXPERT,
    )
    @DebugIt()
    def SetMagnetStrength(self, argin):
        # PROTECTED REGION ID(RingSimulator.SetMagnetStrength) ENABLED START #
        """
        Set a simulated magnet strength (K for quadrupole, H for sextupole...)

        :param argin: 'DevVarDoubleStringArray'
            str[0] = magnet devname 1
            str[1] = magnet devname 2
            ...
            db[0] = strength 1
            db[1] = strength 2
            ...

        :return:None
        """

        for devIdx,devName in enumerate(argin[1]):
                try:
                        strIdx = self.updated_dev.index(devName.lower())
                        self.updated_str[strIdx] = argin[0][devIdx]
                except ValueError:
                        self.updated_str.append(argin[0][devIdx])
                        self.updated_dev.append(devName.lower())

        # PROTECTED REGION END #    //  RingSimulator.SetMagnetStrength

    def is_SetMagnetStrength_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_SetMagnetStrength_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.INIT]
        # PROTECTED REGION END #    //  RingSimulator.is_SetMagnetStrength_allowed

    @command(
        dtype_out='DevVarDoubleStringArray',
        doc_out="str[0] --- str[n] : Names of devices with upd strengths"
                "db[0] --- db[n] : New strength for each magnets (strengths / magnet length)",
        display_level=DispLevel.EXPERT,
    )
    @DebugIt()
    def GetUpdStrengths(self):
        # PROTECTED REGION ID(RingSimulator.GetUpdStrengths) ENABLED START #
        """
            Get which magnet strengths have been updated since previous
            command

        :return:'DevVarDoubleStringArray'
                str[0] --- str[n] : Names of devices with upd strengths
                db[0] --- db[n] : New strength for each magnets (strengths / magnet length)
        """
        print("Entering GetUpdStrengths")
        self.prev_cmd_tp = self.cmd_tp;
        self.cmd_tp =  time.time();

        strVal = self.updated_str
        if self.strengthOffsetMap is not None:
            for i,d in enumerate(self.updated_dev):
                offset = self.strengthOffsetMap.get(d)
                if offset is not None:
                    strVal[i] = strVal[i] - offset

        ret_data = (strVal,self.updated_dev)
        self.updated_dev = []
        self.updated_str = []

        return ret_data

        # PROTECTED REGION END #    //  RingSimulator.GetUpdStrengths

    def is_GetUpdStrengths_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_GetUpdStrengths_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.INIT]
        # PROTECTED REGION END #    //  RingSimulator.is_GetUpdStrengths_allowed

    @command(
        dtype_in='DevVarDoubleArray',
        doc_in="Tunes (H - V)"
               "Chromaticities (H - V)"
               "Emittances (H - V) (Linked to Atx mode)"
               "Closed orbiit H and V (m)"
               "Beta H and V"
               "Dispersion H and V"
               "When used in Atx mode, pinholes related data are added at"
               "the end of data buffer",
        display_level=DispLevel.EXPERT,
    )
    @DebugIt()
    def PushRawData(self, argin):
        # PROTECTED REGION ID(RingSimulator.PushRawData) ENABLED START #
        """
            Used by Matlab script to send simulation result (Tunes,CO,...) 
             to device

        :param argin: 'DevVarDoubleArray'
            Tunes (H - V)
            Chromaticities (H - V)
            Emittances (H - V) (Linked to Atx mode)
            Closed orbiit H and V (m)
            Beta H and V
            Dispersion H and V
            When used in Atx mode, pinholes related data are added at
            the end of data buffer

        :return:None
        """
        print("Received {} data".format(len(argin)))

        self.__tune_h = argin[0]
        self.__tune_v = argin[1]

        self.__chromaticity_h = argin[2]
        self.__chromaticity_v = argin[3]

        self.__emittance_h = argin[4]
        self.__emittance_v = argin[5]

        start_ind = 6
        end_ind = start_ind + RingSimulator.NB_BPM
        self.__closed_orbit_h = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_BPM
        self.__closed_orbit_v = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_BPM
        self.__beta_h = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_BPM
        self.__beta_v = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_BPM
        self.__eta_h = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_BPM
        self.__eta_v = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_BPM
        self.__emittances__bpm__h = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_BPM
        self.__emittances__bpm__v = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_PINHOLES
        self.__emittances__pin_holes__h = argin[start_ind:end_ind]

        start_ind = end_ind
        end_ind = start_ind + RingSimulator.NB_PINHOLES
        self.__emittances__pin_holes__v = argin[start_ind:end_ind]

        start_ind = end_ind
        for i in range(RingSimulator.NB_PINHOLES):
            self.pinholes_data[i][3] = argin[start_ind]
            self.pinholes_data[i][4] = argin[start_ind + 1]
            start_ind = start_ind + 2            

        if len(argin) > RingSimulator.ATLINOPT_NB_DATA:
            for i in range(RingSimulator.NB_PINHOLES):
                self.pinholes_data[i][0] = argin[start_ind]
                self.pinholes_data[i][1] = argin[start_ind + RingSimulator.NB_PINHOLES]
                self.pinholes_data[i][2] = argin[start_ind + (2 * RingSimulator.NB_PINHOLES)]
                start_ind = start_ind + 1

        self.__counter = self.__counter + 1

        # PROTECTED REGION END #    //  RingSimulator.PushRawData

    def is_PushRawData_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_PushRawData_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.INIT]
        # PROTECTED REGION END #    //  RingSimulator.is_PushRawData_allowed

    @command(
        dtype_in='DevVarDoubleArray',
        doc_in="d[0] = turn number"
               "d[1] -- d[x] = H positions"
               "d[X+1] -- d[2X + 1] = V positions",
        display_level=DispLevel.EXPERT,
    )
    @DebugIt()
    def PushTbtRawData(self, argin):
        # PROTECTED REGION ID(RingSimulator.PushTbtRawData) ENABLED START #
        """
            Used by Matlab script to send simulation result in TbT mode
             to device

        :param argin: 'DevVarDoubleArray'
            d[0] = turn number
            d[1] -- d[x] = H positions
            d[X+1] -- d[2X + 1] = V positions

        :return:None
        """
        print("Received {} data  for TbT mode".format(len(argin)));

        nb_turn = int(argin[0])
        nb_data_plane = nb_turn * RingSimulator.NB_BPM;
        # print('size of tbt data')
        # print(nb_data_plane)
        self.__h_positions_tb_t = np.resize(self.__h_positions_tb_t, nb_data_plane)
        self.__v_positions_tb_t = np.resize(self.__v_positions_tb_t, nb_data_plane)

        #print('actual tbt data')
        #print(len(self.__h_positions_tb_t))
        #print(self.__h_positions_tb_t[0:5])
        ind = 0
        for i in range(RingSimulator.NB_BPM):
            for j in range(nb_turn):
                base_ind =  (j * RingSimulator.NB_BPM) + i;
                self.__h_positions_tb_t[ind] = argin[base_ind + 1];
                self.__v_positions_tb_t[ind] = argin[base_ind + nb_data_plane + 1];
                ind = ind + 1;

        self.__h_positions_tb_t = np.resize(self.__h_positions_tb_t, nb_data_plane)
        self.__v_positions_tb_t = np.resize(self.__v_positions_tb_t, nb_data_plane)

        #print('updated tbt data: ')
        #print(self.__h_positions_tb_t[0:5])
        self.__counter = self.__counter + 1

        # PROTECTED REGION END #    //  RingSimulator.PushTbtRawData

    def is_PushTbtRawData_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_PushTbtRawData_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.INIT]
        # PROTECTED REGION END #    //  RingSimulator.is_PushTbtRawData_allowed

    @command(
        dtype_in='DevLong',
        doc_in="Index in pinhole list"
               "0 -> Pinhole in cell 07"
               "1 -> Pinhole in cell 17"
               "2 -> Pinhole in cell 25"
               "3 -> Pinhole in cell 27"
               "4 -> Pinhole in cell 01",
        dtype_out='DevVarDoubleArray',
        doc_out="Pinhole data:"
                "d[0] = sigma_h power 2"
                "d[1] = sigma_v power 2"
                "d[2] = hv_coeff"
                "d[3] = Beam position_h at pinhole place"
                "d[4] = Beam position_v at pinhole place",
        display_level=DispLevel.EXPERT,
    )
    @DebugIt()
    def GetPinholeData(self, argin):
        # PROTECTED REGION ID(RingSimulator.GetPinholeData) ENABLED START #
        """
            Get pinhole related data for one of the 5 pinholes camera
            installed in EBS

        :param argin: 'DevLong'
            Index in pinhole list
            0 -> Pinhole in cell 07
            1 -> Pinhole in cell 17
            2 -> Pinhole in cell 25
            3 -> Pinhole in cell 27
            4 -> Pinhole in cell 01

        :return:'DevVarDoubleArray'
            Pinhole data:
            d[0] = sigma_h power 2
            d[1] = sigma_v power 2
            d[2] = hv_coeff
            d[3] = Beam position_h at pinhole place
            d[4] = Beam position_v at pinhole place
        """
        if argin < 0 or argin >= RingSimulator.NB_PINHOLES:
            mess = "You sent {} as pinhole index while it should be a number between 0 and {}".format(argin,RingSimulator.NB_PINHOLES - 1)
            tango.Except.throw_exception("RingSimulator_WrongArg",mess,"RingSimulator.get_pinhole_data()")

        return self.pinholes_data[argin]

        # PROTECTED REGION END #    //  RingSimulator.GetPinholeData

    def is_GetPinholeData_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_GetPinholeData_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.INIT]
        # PROTECTED REGION END #    //  RingSimulator.is_GetPinholeData_allowed

    @command(
        dtype_in='DevVarDoubleStringArray',
        doc_in="str[0..n] = Magnet name(s)"
               "db[0..n] = Magnet strength(s)",
        display_level=DispLevel.EXPERT,
    )
    @DebugIt()
    def SetMagnetStrengthAll(self, argin):
        # PROTECTED REGION ID(RingSimulator.SetMagnetStrengthAll) ENABLED START #
        """
        Set all simulated magnet strength

        :param argin: 'DevVarDoubleStringArray'
            str[0..n] = Magnet name(s)
            db[0..n] = Magnet strength(s)

        :return:None
        """
        self.updated_dev = []
        self.updated_str = []
        for devIdx,devName in enumerate(argin[1]):
                self.updated_str.append(argin[0][devIdx])
                self.updated_dev.append(devName.lower())

        # PROTECTED REGION END #    //  RingSimulator.SetMagnetStrengthAll

    def is_SetMagnetStrengthAll_allowed(self):
        # PROTECTED REGION ID(RingSimulator.is_SetMagnetStrengthAll_allowed) ENABLED START #
        return self.get_state() not in [DevState.FAULT,DevState.MOVING,DevState.OFF,DevState.RUNNING]
        # PROTECTED REGION END #    //  RingSimulator.is_SetMagnetStrengthAll_allowed

    @command(
    )
    @DebugIt()
    def ResetToDesign(self):
        # PROTECTED REGION ID(RingSimulator.ResetToDesign) ENABLED START #
        """
        Put all correction to 0.

        :return:None
        """

        self.reset_magnet("srmag/m-qf2/c03-e", True)
        self.reset_magnet("srmag/m-qf2/c04-a", True)

        for i in range(1, 33):
            # DQ
            self.reset_magnet("srmag/m-dq1/c%02d-b" % i, True)
            self.reset_magnet("srmag/m-dq1/c%02d-d" % i, True)
            self.reset_magnet("srmag/m-dq2/c%02d-c" % i, True)

            # O
            self.reset_magnet("srmag/m-of1/c%02d-b" % i, True)
            self.reset_magnet("srmag/m-of1/c%02d-d" % i, True)

            # Q
            self.reset_magnet("srmag/m-qd2/c%02d-a" % i, True)
            self.reset_magnet("srmag/m-qd2/c%02d-e" % i, True)
            self.reset_magnet("srmag/m-qd3/c%02d-a" % i, True)
            self.reset_magnet("srmag/m-qd3/c%02d-e" % i, True)
            self.reset_magnet("srmag/m-qd5/c%02d-b" % i, True)
            self.reset_magnet("srmag/m-qd5/c%02d-d" % i, True)
            self.reset_magnet("srmag/m-qf1/c%02d-a" % i, True)
            self.reset_magnet("srmag/m-qf1/c%02d-e" % i, True)
            self.reset_magnet("srmag/m-qf4/c%02d-a" % i, True)
            self.reset_magnet("srmag/m-qf4/c%02d-e" % i, True)
            self.reset_magnet("srmag/m-qf4/c%02d-b" % i, True)
            self.reset_magnet("srmag/m-qf4/c%02d-d" % i, True)
            self.reset_magnet("srmag/m-qf6/c%02d-b" % i, True)
            self.reset_magnet("srmag/m-qf6/c%02d-d" % i, True)
            self.reset_magnet("srmag/m-qf8/c%02d-b" % i, True)
            self.reset_magnet("srmag/m-qf8/c%02d-d" % i, True)

            # S
            self.reset_magnet("srmag/m-sd1/c%02d-a" % i, True)
            self.reset_magnet("srmag/m-sd1/c%02d-e" % i, True)
            self.reset_magnet("srmag/m-sd1/c%02d-b" % i, True)
            self.reset_magnet("srmag/m-sd1/c%02d-d" % i, True)
            self.reset_magnet("srmag/m-sf2/c%02d-a" % i, True)
            self.reset_magnet("srmag/m-sf2/c%02d-e" % i, True)

            # SH
            self.reset_magnet("srmag/m-sh1/c%02d-a" % i, False)
            self.reset_magnet("srmag/m-sh2/c%02d-b" % i, False)
            self.reset_magnet("srmag/m-sh3/c%02d-e" % i, False)

    def reset_magnet(self,name,hasMain):
        ds = tango.DeviceProxy(name)
        strs = ds.Strengths
        if hasMain:
            strs[0] = ds.DesignStrength
        for i in range(1 if hasMain else 0,len(strs)):
            strs[i] = 0
        ds.Strengths = strs

            # PROTECTED REGION END #    //  RingSimulator.ResetToDesign

# ----------
# Run server
# ----------

# PROTECTED REGION ID(RingSimulator.custom_code) ENABLED START #
# PROTECTED REGION END #    //  RingSimulator.custom_code


def main(args=None, **kwargs):
    """Main function of the RingSimulator module."""
    # PROTECTED REGION ID(RingSimulator.main) ENABLED START #
    multiprocessing.set_start_method('spawn')
    return run((RingSimulator,), args=args, **kwargs)
    # PROTECTED REGION END #    //  RingSimulator.main

# PROTECTED REGION ID(RingSimulator.custom_functions) ENABLED START #
# PROTECTED REGION END #    //  RingSimulator.custom_functions


if __name__ == '__main__':
    main()
