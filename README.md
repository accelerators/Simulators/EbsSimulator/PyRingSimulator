# RingSimulator

A Tango class to interface the EBS ring simulator with the Tango world.
This is the python release of the RingSimulator. A C++ one has also been developped. The C++ release 
works with Matlab based ring simulation (using Matlab AT) while the python release works with the python AT
ring simulation

## Cloning

To clone this project, type

```
git clone git@gitlab.esrf.fr:accelerators/Simulators/EbsSimulators/PyRingSimulator.git
```

## Building and Installation

### Dependencies

The project has the following general dependencies.

* Minimum python version: 3.3
