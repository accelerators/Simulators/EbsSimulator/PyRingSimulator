#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the RingSimulator project
#
# Copyright (C): 2019
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the LGPL license.
# See LICENSE.txt for more info.

import os
import sys
from setuptools import setup

setup_dir = os.path.dirname(os.path.abspath(__file__))

# make sure we use latest info from local code
sys.path.insert(0, setup_dir)

readme_filename = os.path.join(setup_dir, 'README.rst')
with open(readme_filename) as file:
    long_description = file.read()

release_filename = os.path.join(setup_dir, 'RingSimulator', 'release.py')
exec(open(release_filename).read())

pack = ['RingSimulator']

setup(name=name,
      version=version,
      description='This class is the interface to the ESRF EBS storage ring simulator.\nSimulation is done using the python Accelerator Toolbox (pyAT) \npackage',
      packages=pack,
      include_package_data=True,
      test_suite="test",
      entry_points={'console_scripts':['RingSimulator = RingSimulator:main']},
      author='taurel',
      author_email='taurel at esrf.fr',
      license='LGPL',
      long_description=long_description,
      url='www.tango-controls.org',
      platforms="Unix Like"
      )
